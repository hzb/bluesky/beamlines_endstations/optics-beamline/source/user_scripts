from beamlinetools.beamline_config import *

def Atest():
    print(f'start of the test macro')

    # mov r.tz -3
    # mov r.tha 0
    # mov r.twt 0

    r.twt.settle_time = 0.2
    # scan r.twt -.15 .15 30

    # r.twt.settle_time = 0.5
    # scan r.twt -.15 .15 30

    # r.twt.settle_time = 1.0
    # scan r.twt -.15 .15 30

    # r.twt.settle_time = 2.0
    # scan r.twt -.15 .15 30

    # r.twt.settle_time = 5.0
    # scan r.twt -.15 .15 30

    # r.twt.settle_time = 10.0
    # scan r.twt -.15 .15 30

    print(f'finish of the test macro')

    