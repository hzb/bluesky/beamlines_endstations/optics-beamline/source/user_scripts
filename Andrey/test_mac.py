from beamlinetools.beamline_config import *

def andrey():
    mov r.twt 0
    scan r.twt 0 1 20
    return

def Atest():

    print(f'start of the test macro')

    mov r.tz -3
    mov r.tha 0
    mov r.twt 0

    r.twt.settle_time = 0.4
    scan r.twt -.5 .5 200
    
    r.twt.settle_time = 0.5
    scan r.twt -.5 .5 200

    r.twt.settle_time = 1.0
    scan r.twt -.5 .5 200

    r.twt.settle_time = 2.0
    scan r.twt -.5 .5 200

    r.twt.settle_time = 5.0
    scan r.twt -.5 .5 200

    r.twt.settle_time = 10.0
    scan r.twt -.5 .5 200

    print(f'finish of the test macro')

    return

# def loop_range():
#     for i in range(0,10):
#         print(i)
#         # run_plan('scan motor -1 1 10')
#         RE(scan_plan([kth1],r.twt,-1, 1, 100))
#     return

def loop_list():
    number_points = [1,2,3,4,5]
    for position in number_points:
        print(position)
        # dscan motor 0  20


def Amac():
    print(f'loadin "test_mac.py" file')
#    load_user_script('Andrey/test_mac.py')
    return

# def AIo():
#     print(f'movin to Io positions')
#     run_plan('mov r.tha 0')
#     #run_plan('mov r.twt 0')
#     #run_plan('mov r.tz -3')
#     return

    